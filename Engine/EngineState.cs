﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoBreakout.Engine
{
    /// <summary>
    /// An extension of EngineGroup but intended for state so that you can have reference to the main game.
    /// This is an abstract class so make it is extended off.
    /// </summary>
    public abstract class EngineState : EngineGroup
    {
        protected GameRunner game;

        public EngineState(GameRunner game)
        {
            this.game = game;
        }

        public abstract void Create();
        public override void UnloadContent()
        {
            base.UnloadContent();
            game = null;
        }
    }
}
