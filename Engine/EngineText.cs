﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace MonoBreakout.Engine
{
    /// <summary>
    /// Spritefont wrapper
    /// </summary>
    public class EngineText : EngineObject
    {
        protected SpriteFont font;
        protected string filename;
        protected Vector2 position;
        protected string text;
        protected Color color;
        protected SpriteEffects effects;
        protected Vector2 pivot;
        protected float rotation;
        protected float scale;
        protected float depth;

        public Vector2 Position         { get { return position; } set { position = value; } }
        public SpriteFont Font          { get { return font; } }
        public String Text              { get { return text; } set { text = value; } }
        public Color Colour             { get { return color; } set { color = value; } }
        public SpriteEffects Effects    { get { return effects; } set { effects = value; } }
        public Vector2 Pivot            { get { return pivot; } set { pivot = value; } }
        public float Rotation           { get { return rotation; } set { rotation = value; } }
        public float Scale              { get { return scale; } set { scale = value; } }
        public float Depth              { get { return depth; } set { depth = value; } }

        public EngineText(float x, float y, string filename)
            : base ()
        {
            this.filename   = filename;
            this.position   = new Vector2(x, y);
            this.color      = Color.White;
        }

        public override void LoadContent(ContentManager content)
        {
            font = content.Load<SpriteFont>(filename);
        }

        public override void UnloadContent()
        {
            throw new NotImplementedException();
        }

        public override void Update(GameTime gameTime)
        {
            throw new NotImplementedException();
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(font, text, position, color, rotation, pivot, scale, effects, depth);
        }
    }
}
