﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace MonoBreakout.Engine
{
    /// <summary>
    /// 2D sprite to be displayed in the engine.
    /// TODO: Add properties to get access to the variables
    /// </summary>
    public class EngineSprite : EngineObject
    {
        protected Texture2D     texture;
        protected int           width;
        protected int           height;
        protected string        filename;
        protected Rectangle     bounds;

        public Vector2          position;
        public Vector2          pivot;
        public Color            color;
        public float            rotation;
        public float            scale;
        public float            layerDepth;
        public SpriteEffects    effects;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        public EngineSprite(string filename)
        {
            this.position = Vector2.Zero;
            this.filename = filename;
            Initialise();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="filename"></param>
        public EngineSprite(float x, float y, string filename)
        {
            position = new Vector2(x, y);
            this.filename = filename;
            Initialise();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="position"></param>
        /// <param name="filename"></param>
        public EngineSprite(Vector2 position, string filename)
        {
            this.position = position;
            this.filename = filename;
            Initialise();
        }

        private void Initialise()
        {
            pivot       = Vector2.Zero;
            color       = Color.White;
            rotation    = 0.0f;
            scale       = 1.0f;
            layerDepth  = 1.0f;
            effects     = SpriteEffects.None;
        }

        public override void LoadContent(ContentManager content)
        {
            texture     = content.Load<Texture2D>(filename);
            width       = texture.Width;
            height      = texture.Height;
            bounds      = new Rectangle((int)position.X, (int)position.Y, width, height);
        }

        public override void UnloadContent()
        {
            texture.Dispose();
        }

        public override void Update(GameTime gameTime)
        {
            UpdateBounds();
        }

        protected void UpdateBounds()
        {
            bounds.X = (int)position.X;
            bounds.Y = (int)position.Y;
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, position, new Rectangle(0, 0, width, height),
                color, rotation, pivot, scale, effects, layerDepth);
        }

        #region Properties
        public Texture2D Texture { get { return texture; } }
        public int Width { get { return width; } }
        public int Height { get { return height; } }
        public Rectangle Bounds { get { return bounds; } }
        #endregion
    }
}
