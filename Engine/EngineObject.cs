﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoBreakout.Engine
{
    /// <summary>
    /// Abstract class in which everything in the engine exists from.
    /// </summary>
    /// <example>
    /// EngineSprite : EngineObject
    /// {
    ///     public override void Update(GameTime gameTime)
    ///     {
    ///         //do stuff
    ///     }
    /// }
    /// </example>
    public abstract class EngineObject
    {
        #region Variables
        /// <summary>
        /// Variable for if the object exists.
        /// </summary>
        protected bool active   = true;

        /// <summary>
        /// Variable for if the object is visible.
        /// </summary>
        protected bool visible  = true;
        #endregion

        #region Properties
        /// <summary>
        /// A property to set or get the Active of the object
        /// </summary>
        public bool Active
        {
            get
            {
                return active;
            }
            set
            {
                active = value;
            }
        }

        /// <summary>
        /// A property to set or get the Visibility of the object
        /// </summary>
        public bool Visible
        {
            get
            {
                return visible;
            }
            set
            {
                visible = value;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Helper function. Quickly kills the object.
        /// </summary>
        public virtual void Kill()
        {
            active  = false;
            visible = false;
        }

        /// <summary>
        /// Helper function. Quickly revives the object.
        /// </summary>
        public virtual void Revive()
        {
            active  = true;
            visible = true;
        }
        #endregion

        #region Abstract Methods
        /// <summary>
        /// Loads any content via the given ContentManager
        /// </summary>
        /// <param name="content"></param>
        public abstract void LoadContent(ContentManager content);

        /// <summary>
        /// Unloads any loaded content and disposes of it.
        /// </summary>
        public abstract void UnloadContent();

        /// <summary>
        /// Updates the object
        /// </summary>
        /// <param name="gameTime">The current time/game time that has passed</param>
        public abstract void Update(GameTime gameTime);

        /// <summary>
        /// Draws the object
        /// </summary>
        /// <param name="gameTime">The current time/game time that has passed</param>
        /// <param name="spriteBatch">The SpriteBatch instance that is used to draw the object</param>
        public abstract void Draw(GameTime gameTime, SpriteBatch spriteBatch);
        #endregion
    }
}
