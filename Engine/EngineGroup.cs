﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoBreakout.Engine
{
    /// <summary>
    /// A group controller consisting of an EngineObject so that it can be added to other groups for example.
    /// </summary>
    public class EngineGroup : EngineObject
    {
        protected List<EngineObject> members;
        private int membersCount;

        /// <summary>
        /// Creates and instantiates a new group
        /// </summary>
        public EngineGroup()
        {
            members         = new List<EngineObject>();
            membersCount    = 0;
        }

        /// <summary>
        /// Creates and instantiates a new group
        /// </summary>
        /// <param name="capacity">The maximum amount this group can contain</param>
        public EngineGroup(int capacity)
        {
            members = new List<EngineObject>(capacity);
        }

        public void AddMember(EngineObject member)
        {
            if (members.Contains(member)) { return; }
            members.Add(member);
            membersCount++;
        }

        public void RemoveMember(EngineObject member)
        {
            if (!members.Contains(member)) { return; }
            members.Remove(member);
            membersCount--;
        }

        public override void LoadContent(ContentManager content)
        {
            for (int i = 0; i < membersCount; i++)
            {
                members[i].LoadContent(content);
            }
        }

        public override void UnloadContent()
        {
            for (int i = 0; i < membersCount; i++)
            {
                members[i].UnloadContent();
            }
        }

        public override void Update(GameTime gameTime)
        {
            for (int i = 0; i < membersCount; i++)
            {
                if (members[i].Active)
                {
                    members[i].Update(gameTime);
                }
            }
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            for (int i = 0; i < membersCount; i++)
            {
                if (members[i].Visible)
                {
                    members[i].Draw(gameTime, spriteBatch);
                }
            }
        }

        public int Count
        {
            get
            {
                return membersCount;
            }
        }
    }
}
