# README #

MonoBreak is a clone of the Breakout game by Atari but built entirely in MonoGame with open source assets. Source code is used for others to freely learn from.
Breakout is a perfect game to clone when learning how a specific game engine or framework functions. Game instantiation, asset loading, physics, input and sound!

### What is this repository for? ###

* Hosts MonoBreak code.
* 0.1

### How do I get set up? ###

* Built in Visual Studio 2015.
* Open solution from Source Control and fork this repository.

### Contribution guidelines ###

* No contributions available. Please fork and experiment :)

### Who do I talk to? ###

* Joshua Godden