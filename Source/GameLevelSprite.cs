﻿using MonoBreakout.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoBreakout.Source
{
    public class GameLevelSprite : EngineSprite
    {
        private int index;
        private int row;
        private int column;
        private bool tile;
        
        /// <summary>
        /// Creates and instantiates a new level sprite
        /// </summary>
        /// <param name="index"></param>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <param name="tile"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="filename"></param>
        public GameLevelSprite(int index, int row, int column, bool tile, float x, float y, string filename)
            : base (x, y, filename)
        {
            this.index  = index;
            this.row    = row;
            this.column = column;
            this.tile   = tile;
        }

        public override string ToString()
        {
            return "Index: " + index + " , row: " + row + ", column: " + column + ", tile: " + tile;
        }

        #region Properties
        public int Index
        {
            get
            {
                return index;
            }
        }
        public int Row
        {
            get
            {
                return row;
            }
        }
        public int Column
        {
            get
            {
                return column;
            }
        }
        public bool Tile
        {
            get
            {
                return tile;
            }
        }
        #endregion
    }
}
