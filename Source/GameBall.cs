﻿using MonoBreakout.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace MonoBreakout.Source
{
    public class GameBall : EngineSprite
    {
        public enum BallState
        {
            STATIC,
            MOVING,
            DEAD
        };
        private BallState state;
        private float xSpeed;
        private float ySpeed;

        public BallState State
        {
            get { return state;     }
            set { state = value;    }
        }

        /// <summary>
        /// Creates and instantiates the ball
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public GameBall(float x, float y)
            : base (x, y, "ballBlue")
        {
            state = BallState.STATIC;
            ySpeed = -500;
            xSpeed = 100;
        }

        /// <summary>
        /// Launches the ball
        /// </summary>
        public void Launch()
        {
            state = BallState.MOVING;
        }

        public void FlipX()
        {
            xSpeed = -xSpeed;
        }

        public void FlipY()
        {
            ySpeed = -ySpeed;
        }

        public override void Update(GameTime gameTime)
        {
            float delta = (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (state == BallState.MOVING)
            {
                position.Y += (ySpeed * delta);
                position.X += (xSpeed * delta);
            }
            base.Update(gameTime);
        }

        public float XSpeed
        {
            get
            {
                return xSpeed;
            }
            set
            {
                xSpeed = value;
            }
        }
    }
}
