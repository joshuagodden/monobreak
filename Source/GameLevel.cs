﻿using MonoBreakout.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;

namespace MonoBreakout.Source
{
    public class GameLevel : EngineGroup
    {
        private int[,] data;
        private EngineSprite[,] map;

        /// <summary>
        /// Creates a new game level, we want to have a set capacity here so we know the level only
        /// needs to process so much and save memory.
        /// </summary>
        /// <param name="capacity">The max amount of objects in a level</param>
        public GameLevel()
            : base ()
        {
            data = new int[15, 20] {
                { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                { 1, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 1},
                { 1, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 1},
                { 1, 4, 0, 4, 0, 4, 0, 4, 0, 4, 0, 4, 0, 4, 0, 4, 0, 4, 0, 1},
                { 1, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 5, 0, 1},
                { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
                { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
                { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
                { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
                { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
                { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
                { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
                { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
                { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
                { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}
            };
        }

        public override void LoadContent(ContentManager content)
        {
            for (int x = 0; x < data.GetLength(0); x++)
            {
                bool instantiate = false;
                string filename = "";
                bool tile = false;

                for (int y = 0; y < data.GetLength(1); y++)
                {
                    int id = data[x, y];
                    switch (id)
                    {
                        default :
                        case 0  :
                            instantiate = false;
                            break;
                        case 1:
                            filename    = "element_grey_square";
                            instantiate = true;
                            tile = true;
                            break;
                        case 2:
                            filename    = "element_blue_rectangle";
                            instantiate = true;
                            tile = false;
                            break;
                        case 3:
                            filename    = "element_red_rectangle";
                            instantiate = true;
                            tile = false;
                            break;
                        case 4:
                            filename    = "element_yellow_rectangle";
                            instantiate = true;
                            tile = false;
                            break;
                        case 5:
                            filename    = "element_green_rectangle";
                            instantiate = true;
                            tile = false;
                            break;
                    }
                    if (instantiate && filename != "")
                    {
                        int index = y + (x * data.GetLength(0));
                        Console.WriteLine(index);
                        AddMember(new GameLevelSprite(index, x, y, tile, (y * 32), (x * 32), filename));
                    }
                }
            }
            base.LoadContent(content);
        }

        /// <summary>
        /// Checks to see if a sprite intersects the level
        /// </summary>
        /// <param name="sprite"></param>
        /// <returns></returns>
        public GameLevelSprite Intersects(EngineSprite sprite)
        {
            for (int i = 0; i < Count; i++)
            {
                GameLevelSprite spr = (GameLevelSprite)members[i];
                if (spr.Active && spr.Visible)
                {
                    if (sprite.Bounds.Intersects(spr.Bounds))
                    {
                        return spr;
                    }
                }
            }
            return null;
        }
    }
}
