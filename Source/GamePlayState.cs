﻿using MonoBreakout.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace MonoBreakout.Source
{
    public class GamePlayState : EngineState
    {
        private GamePaddle      paddle;
        private GameBall        ball;
        private GameLevel       level;

        private KeyboardState   previousState;
        private KeyboardState   currentState;

        public GamePlayState(GameRunner game)
            : base (game)
        { }

        public override void Create()
        {
            ball    = new GameBall(-60, -60);
            paddle  = new GamePaddle(36, 440, ball);
            level   = new GameLevel();
            AddMember(paddle);
            AddMember(ball);
            AddMember(level);
        }

        public override void LoadContent(ContentManager content)
        {
            base.LoadContent(content);
        }

        public override void Update(GameTime gameTime)
        {
            HandleInput(gameTime);
            HandleCollisions(gameTime);
            base.Update(gameTime);
        }

        /// <summary>
        /// Handles input for this state
        /// </summary>
        private void HandleInput(GameTime gameTime)
        {
            float delta     = (float)gameTime.ElapsedGameTime.TotalSeconds;
            currentState    = Keyboard.GetState();

            if (currentState.IsKeyDown(Keys.Right))
            {
                if (paddle.position.X <= (GameRunner.WINDOW_WIDTH - (paddle.Width + 40)))
                {
                    paddle.position.X += (paddle.Speed * delta);
                }
            }
            if (currentState.IsKeyDown(Keys.Left))
            {
                if (paddle.position.X >= 40)
                {
                    paddle.position.X -= paddle.Speed * delta;
                }
            }
            if (previousState.IsKeyDown(Keys.Space) && currentState.IsKeyUp(Keys.Space))
            {
                if (ball.State == GameBall.BallState.STATIC)
                {
                    ball.Launch();
                }
            }

            if (ball.State == GameBall.BallState.STATIC)
            {
                ball.position.X = paddle.position.X + ((paddle.Width / 2) - (ball.Width / 2));
                ball.position.Y = paddle.position.Y - 24;
            }
            previousState = currentState;
        }

        /// <summary>
        /// Checks collisions for this state
        /// </summary>
        private void HandleCollisions(GameTime gameTime)
        {
            switch (paddle.CheckCollision(ball))
            {
                case -1 : break;
                case 0  : ball.XSpeed = -100;    ball.FlipY(); break;
                case 1  : ball.XSpeed = -50;     ball.FlipY(); break;
                case 2  : ball.XSpeed = 100;     ball.FlipY(); break;
            }

            if (ball.position.Y > GameRunner.WINDOW_HEIGHT)
            {
                ball.State = GameBall.BallState.STATIC;
            }

            //- handles the level collisions
            GameLevelSprite levelSprite = level.Intersects(ball);
            if (levelSprite != null)
            {
                Console.WriteLine(levelSprite);
                if (levelSprite.Tile)
                {
                    //- top or side?
                    if (levelSprite.Row == 0)
                    {
                        ball.FlipY();
                    }
                    else
                    {
                        ball.FlipX();
                    }
                }
                else
                {
                    levelSprite.Kill();
                    ball.FlipY();
                }
            }

            /*
            paddle.DebugCollisions(ball);
            if (ball.Bounds.Intersects(paddle.Bounds))
            {
                ball.FlipY();
            }*/

            //- ball
            /*
            int ballCollision = paddle.CheckCollision(ball);
            if (ballCollision != 1)
            {
                ball.FlipY();
                if (ballCollision == 0)
                {
                    ball.XSpeed = -50;
                }
                if (ballCollision == 1)
                {
                    ball.XSpeed = 10;
                }
                if (ballCollision == 2)
                {
                    ball.XSpeed = 50;
                }
            }*/
        }
    }
}
