﻿using MonoBreakout.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace MonoBreakout.Source
{
    /// <summary>
    /// Game paddle should just handle the paddle only,
    /// move controls to the state rather than the sprite
    /// </summary>
    public class GamePaddle : EngineSprite
    {
        private float speed;
        private Rectangle leftBounds;
        private Rectangle centerBounds;
        private Rectangle rightBounds;

        private readonly int QUAD_WIDTH     = 26;
        private readonly int QUAD_HEIGHT    = 24;

        public float Speed
        {
            get
            {
                return speed;
            }
        }

        /// <summary>
        /// Creates and instantiates the Game Paddle
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="ball"></param>
        public GamePaddle(float x, float y, GameBall ball)
            : base(x, y, "paddleBlu")
        {
            Console.WriteLine("Creating Paddle");
            speed = 500;
        }

        /**
            Bat size is 104 x 24
            26 quad in width
            26 for left
            52 for center
            26 for right
            x 24 in height
        */
        public override void LoadContent(ContentManager content)
        {
            base.LoadContent(content);

            //- for memory it would be best to hardcode rather than calculate
            leftBounds      = new Rectangle(0, 0, 26, 24);
            centerBounds    = new Rectangle(0, 0, 52, 24);
            rightBounds     = new Rectangle(0, 0, 26, 24);
        }

        public override void Update(GameTime gameTime)
        {
            float delta     = (float)gameTime.ElapsedGameTime.TotalSeconds;
            leftBounds.X    = (int)position.X;
            leftBounds.Y    = (int)position.Y;
            centerBounds.X  = (int)position.X + QUAD_WIDTH;
            centerBounds.Y  = (int)position.Y;
            rightBounds.X   = (int)position.X + (QUAD_WIDTH * 3);
            rightBounds.Y   = (int)position.Y;
            base.Update(gameTime);
        }

        public int CheckCollision(EngineSprite spr)
        {
            if (spr.Bounds.Intersects(leftBounds))
            {
                return 0;
            }
            else if (spr.Bounds.Intersects(centerBounds))
            {
                return 1;
            }
            else if (spr.Bounds.Intersects(rightBounds))
            {
                return 2;
            }
            else
            {
                return -1;
            }
        }
    }
}
